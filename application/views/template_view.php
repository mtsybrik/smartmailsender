<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<title>Умная Почта Сервер</title>
		<link rel="stylesheet" type="text/css" href="/css/style.css" />
	</head>
	<!-- Body Starts Here -->
	<body id="body" style="overflow:hidden;">
		<div id="content">
					<div class="box">
						<?php include 'application/views/'.$content_view; ?>

					</div>
					<br class="clearfix" />
				</div>
				<br class="clearfix" />
			</div>
		</body>
		<!-- Body Ends Here -->
</html>
