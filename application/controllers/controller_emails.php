<?php

/**
 * Created by PhpStorm.
 * User: ntsybrik
 * Date: 9/13/16
 * Time: 7:01 PM
 */
class controller_emails extends Controller
{
    public function action_send(){
        $model_emails = new model_emails;
        $messages = $model_emails->receive_data();
        $model_emails->send_emails($messages);
    }
}
