<?php

// Connect core files
require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';


//Form parse data model

require_once 'core/route.php';

//Load Registry classes

spl_autoload_register(function ($class_name) {
    include_once 'registry/' . $class_name . '.php';
});


Route::start(); // start routing mechanism