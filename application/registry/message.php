<?php
/**
 * Created by PhpStorm.
 * User: ntsybrik
 * Date: 9/16/16
 * Time: 4:57 PM
 */

class Message
{
    private $name;
    private $email;
    private $message;
    private $giod;

    public function __construct($messageGuid,$messageName,$messageEmail,$messageMessage)
    {
        $this->name = $messageName;
        $this->email=$messageEmail;
        $this->message=$messageMessage;
        $this->guid = $messageGuid;
    }

    public function getName()
    {
        return $this->name;
    }
    public function getEmail(){
        return $this->email;
    }
    public function getMessage(){
        return $this->message;
    }
    public function getGuid(){
        return $this->guid;
    }
}