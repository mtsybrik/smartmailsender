<?php
/**
 * Created by PhpStorm.
 * User: ntsybrik
 * Date: 9/13/16
 * Time: 10:30 PM
 */

class model_emails extends Model
{
    public function receive_data()
    {
        $conn = new mysqli('localhost','root','root','smartmail_back','3307');
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        //Query that selects all messages older than 10 mins from now and sent is false
        $sql = "(Select id, name, email, message from messages WHERE date_created < DATE_SUB(NOW(),INTERVAL 10 MINUTE) AND sent=0 )";
        //Set charset to support Russian
        $conn->set_charset("utf8");
        if ($stmt = $conn->prepare($sql)) {

            /* Запустить выражение */
            $stmt->execute();

            /* Определить переменные для результата */
            $stmt->bind_result($guid, $name, $email, $message);

            /*Массив для сохранения обьектов*/
            $messages = array();
            /* Выбрать значения */
            while ($stmt->fetch()){

            $messages[] = new Message($guid, $name, $email, $message);
            }
        }
        /* Завершить запрос */
        $stmt->close();

        /*Возвращаем массив в контроллер*/

        return $messages;
    }
    public static function send_emails($messages)
    {

        foreach ($messages as &$message) {
            $email = $message->getEmail();
            $domain = strstr($email, '@');
            $domain1 = substr($domain, 1);
            $conn = new mysqli('localhost', 'root', 'root', 'smartmail_back', '3307');
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            if ($domain1 == 'mail.ru' || $domain1 == 'rambler.ru' || $domain1 == 'yandex.ru') {
                // Сообщение
                $message1 = $message->getName() . ", добрый день! Мы не можем вам ничем помочь, т.к. Не работаем с клиентами " . $domain1 . ". ";

                // Отправляем
                $mail = mail($email, 'К сожалению, мы не можем вам помочь', $message1);
                if($mail){
                    //Query that updates field sent for given message
                    $sql = "UPDATE `messages` SET `sent`='1' WHERE `id`='" . $message->getGuid() . "'";
                    if ($stmt = $conn->prepare($sql)) {

                        /* Запустить выражение */
                        $stmt->execute();

                        /* Завершить запрос */
                        $stmt->close();
                    }
                }else{
                    echo "Mail sending failed.";
                }
            } else {
                // Сообщение
                $message1 = $message->getName() . ', добрый день! Очень рады, что вы выбрали ' . $domain1 . '. Ваш запрос: "' . $message->getMessage() . '" будет обработан в ближайшее время. Удачи!';

                // Отправляем
                $mail = mail($email, 'Всё отлично', $message1);
                if($mail) {
                    //Query that updates field sent for given message
                    $sql = "UPDATE `messages` SET `sent`='1' WHERE `id`='" . $message->getGuid() . "'";
                    if ($stmt = $conn->prepare($sql)) {

                        /* Запустить выражение */
                        $stmt->execute();

                        /* Завершить запрос */
                        $stmt->close();
                    }
                }
            }

            /*Очистить переменную от текущего обьекта*/
            unset($message);
        }
    }
}